# Haskell: From Beginner to Intermediate #2

## FP AMS 17/02/2021 

---

# Goal

![inline](learning-curve.png)

---

# Overall Planning

1. **Basics**
2. Functors, Applicative Functors, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50%](book-cover.jpg)

---

# Last Week

- Simple functions
- Lists, ranges, list comprehensions, and tuples
- Types, type variables, and type classes

--- 

# This Week

- Function syntax
- Recursion

---

# Function Syntax

## Pattern Matching

```haskell
tell :: (Show a) => [a] -> String
tell [] = "The list is empty"
tell (x : []) = "The list has one element: " ++ show x
tell (x : y : []) =
  "The list has two elements: "
    ++ show x
    ++ " and "
    ++ show y
tell (x : y : _) =
  "This list is long. The first two elements are: "
    ++ show x
    ++ " and "
    ++ show y
```

---

# Function Syntax

## Guards

```haskell
bmiTell :: (RealFloat a) => a -> a -> String  
bmiTell weight height
    | weight / height ^ 2 <= 18.5 = "underweight"
    | weight / height ^ 2 <= 25.0 = "normal"
    | weight / height ^ 2 <= 30.0 = "overweight"
    | otherwise = "obese"
```

---

# Function Syntax

## Where Bindings

```haskell
bmiTell :: (RealFloat a) => a -> a -> String
bmiTell weight height
    | bmi <= 18.5 = "underweight"
    | bmi <= 25.0 = "normal"
    | bmi <= 30.0 = "overweight"
    | otherwise = "obese"
    where bmi = weight / height ^ 2
```

---

# Function Syntax

## Let Bindings

```haskell
cylinder :: (RealFloat a) => a -> a -> a
cylinder r h =
    let sideArea = 2 * pi * r * h
        topArea = pi * r ^2
    in  sideArea + 2 * topArea
```

---

# Function Syntax

## Case Expressions

```haskell
describeList :: [a] -> String
describeList xs =
  "The list is " ++ case xs of
                      [] -> "empty."
                      [x] -> "a singleton list."
                      xs -> "a longer list."
```

---

# Recursion

```haskell
factorial :: (Integral a) => a -> a  
factorial 0 = 1  
factorial n = n * factorial (n - 1)  
```

---

# Next Week

- Higher-order functions
- Modules

---

# Homework

- Towers of Hanoi
