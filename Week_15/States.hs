{-# OPTIONS_GHC -Wall #-}
module States where

data Tree a = Leaf a | Node (Tree a) (Tree a) deriving (Show)

-- simple recursion
size :: Tree a -> Int
size (Leaf _) = 1
size (Node l r) = (size l) + (size r)

-- tree example
charTree_1 :: Tree Char
charTree_1 = Node (Node (Leaf 'a') (Node (Leaf 'b') (Leaf 'c'))) (Leaf 'd')

-- reLabel classic
reLabel :: Tree a -> Int -> (Tree (Int,a), Int)
reLabel (Leaf x) n  = (Leaf (n,x), n+1)
reLabel (Node l r) n = (Node l' r', n2) where
                       (l', n1) = reLabel l n
                       (r', n2) = reLabel r n1

type WithCounter a = Int -> (a,Int)

-- counting leaves from some starting number
countLabel :: Tree a -> WithCounter (Tree a)
countLabel (Leaf x) n  = (Leaf x, n+1)
countLabel (Node l r) n = (Node l' r', n2) where
                       (l', n1) = countLabel l n
                       (r', n2) = countLabel r n1

-- passing count after implementing change
next :: WithCounter a -> (a -> WithCounter b) -> WithCounter b
next f g = (\i -> let (x, i') = f i in g x i')

-- initialising count
puree :: a -> WithCounter a
puree x = (\i -> (x,i))

-- newLabel with counter
newLabel :: Tree a -> WithCounter (Tree (Int,a))
newLabel (Leaf x)  = (\i -> (Leaf (i,x),i+1))
newLabel (Node l r) = newLabel l `next` (\l' ->
                      newLabel r `next` (\r' ->
                      puree (Node l' r'))) 

-------------- homework --------------                    
type State s a = s -> (a,s)

sNext :: State s a -> (a -> State s b) -> State s b
sNext f g = (\i -> let (x, i') = f i in g x i')

sPure :: a -> State s a
sPure x = (\i -> (x,i))

statLabel :: Tree a -> State Int (Tree (Int,a))
statLabel (Leaf x)  = (\i -> (Leaf (i,x),i+1))
statLabel (Node l r) = statLabel l `sNext` (\l' ->
                       statLabel r `sNext` (\r' ->
                       sPure (Node l' r'))) 

putFront, plusPlus :: [a] -> [a] -> [a]
putFront [] l2 = l2
putFront (x:xs) l2 = putFront xs (x:l2)

plusPlus l1 l2 = putFront (reverse l1) l2

-- pure for lists
singleton :: a -> [a]
singleton x = [x]

-- bind for lists
listNext :: [a] -> (a -> [b]) -> [b]
listNext ls f = concat (fmap f ls)

