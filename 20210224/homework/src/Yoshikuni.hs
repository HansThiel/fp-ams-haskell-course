module Yoshikuni where

-- Exercise 1

fun1 :: [Integer] -> Integer
fun1 [] = 1
fun1 (x : xs)
  | even x = (x - 2) * fun1 xs
  | otherwise = fun1 xs

fun1' :: [Integer] -> Integer
fun1' = product . map (subtract 2) . filter even

-- Exercise 2

-- Hint: For this problem you may wish to use the functions `iterate` and
-- `takeWhile`. Look them up in the Prelude documentation to see what they do.

fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n
  | even n = n + fun2 (n `div` 2)
  | otherwise = fun2 (3 * n + 1)

fun2' :: Integer -> Integer
fun2' = sum . filter even . takeWhile (/=1) . iterate (\x -> if even x then x `div` 2 else 3 * x + 1) 

-- n = 2
-- 2 + fun2 1 
-- 2 + 0

-- [2, 1, 4, 2, ...]
-- [2]
-- 0


-- n = 3
-- fun2 10
-- 10 + fun2 5
-- 10 + fun2 16
-- 10 + 16 + fun2 8
-- 10 + 16 + 8 + fun2 4
-- 10 + 16 + 8 + 4 + fun2 2
-- 10 + 16 + 8 + 4 + 2 + fun2 1
-- 10 + 16 + 8 + 4 + 2 + 0

-- [3, 10, 5, 16, 8, 4, 2, 1, 4, 2, 1, 4, ...]
-- [3, 10, 5, 16, 8, 4, 2]
-- [10, 16, 8, 4, 2]


-- n = 4
-- 4 + fun2 2
-- 4 + 2 + fun2 1
-- 4 + 2 + 0

-- n = 5
-- fun2 16
-- 16 + fun2 8
-- 16 + 8 + fun2 4
-- 16 + 8 + 4 + fun2 2
-- 16 + 8 + 4 + 2 + fun2 1
-- 16 + 8 + 4 + 2 + 0

-- n = 6
-- 6 + fun2 3
-- 6 + fun2 10
-- 6 + 10 + fun2 5
-- 6 + 10 + fun2 16
-- 6 + 10 + 16 + fun2 8
-- 6 + 10 + 16 + 8 + fun2 4
-- 6 + 10 + 16 + 8 + 4 + fun2 2
-- 6 + 10 + 16 + 8 + 4 + 2 + fun2 1
-- 6 + 10 + 16 + 8 + 4 + 2 + 0



-- Excercise 3

xor :: [Bool] -> Bool
-- xor = odd . foldl (\acc x-> if x then acc + 1 else acc) 0 
xor = foldl (\acc x -> if (acc && x) then False else acc || x) False


map' :: (a -> b) -> [a] -> [b]
map' f = foldr (\x acc -> f x : acc) []

myFoldl :: (a -> b -> a) -> a -> [b] -> a
myFoldl f base xs = foldr (\x acc -> f acc x) base xs