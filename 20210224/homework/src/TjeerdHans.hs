module TjeerdHans where

-- https://www.cis.upenn.edu/~cis194/spring13/hw/04-higher-order.pdf
-- Exercise 1
--1
fun1 :: [Integer] -> Integer
fun1 [] = 1
fun1 (x : xs)
  | even x = (x - 2) * fun1 xs
  | otherwise = fun1 xs

fun1' :: [Integer] -> Integer
fun1' [] = 1
fun1' xs = foldl (\acc x -> if even x then acc * (x -2) else acc) 1 xs

fun1'' :: [Integer] -> Integer
--fun1'' [] = 1
-- fun1'' xs = product $ map (subtract 2) $ filter (even) xs
fun1'' = product . map (subtract 2) . filter (even)

-- (-2) doesn't work because -2 is interpreted as a value. (+(-2)) would also work. https://stackoverflow.com/a/28858218

--2
fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n
  | even n = n + fun2 (n `div` 2)
  | otherwise = fun2 (3 * n + 1)

fun2' :: Integer -> Integer
-- function application
-- fun2' n = sum $ filter even $ takeWhile (/=1) $ iterate (\x -> if even x then (x `div` 2) else (3*x+1)) n
-- function composition
fun2' = sum . filter even . takeWhile (/=1) $ iterate (\x -> if even x then (x `div` 2) else (3*x+1))


-- Exercise 3: More folds!

-- 1.  Implement a function
xor :: [Bool] -> Bool
-- which returns True if and only if there are an odd number of True values contained in the input list. 
-- It does not matter how many False values the input list contains. 
-- For example, 
-- xor [False, True, False] == True
-- xor [False, True, False, False, True] == False
-- Your solution must be implemented using a fold.

xor [] = False
xor xs = foldl (\acc x -> not (acc && x)) False xs $ filter (\x->x) xs

-- 2.  Implement map as a fold. That is, complete the definition
map' :: (a -> b) -> [a] -> [b]
-- map’ f = foldr ...
-- in such a way that map’ behaves identically to the standard map function.
map' f = foldr (\x acc -> f x : acc) []