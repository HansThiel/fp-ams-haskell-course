# Haskell: From Beginner to Intermediate #9

## Applicative Functors

### FP AMS 07/04/2021 

---

![right 100%](learning-curve.png)

# Overall Planning

1. Basics
2. Functors, **Applicative Functors**, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50%](book-cover.jpg)![inline 64%](book-of-monads.png)

---

# Applicative Typeclass

```haskell
class Functor f => Applicative f where
  pure :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b 
  liftA2 :: (a -> b -> c) -> f a -> f b -> f c
```

Note: we're mainly ignoring `liftA2`

---

![](source.gif)

---

# Defining `liftA2` with `<$>` and `<*>` (1/4)

```haskell
liftA2 :: Applicative f => (a -> b -> c) -> f a -> f b -> f c
liftA2 f x y = f <$> x <*> y
```

---

# Defining `liftA2` with `<$>` and `<*>` (2/4)

`(<$>)` and `(<*>)`:
- have the same precedence (4)
- are both left-associative

Thus if we add parentheses:

- `(f <$> x) <*> y`

---

# Defining `liftA2` with `<$>` and `<*>` (3/4)

Let's follow the types:

- `f :: a -> b -> c` 
- `a -> b -> c = a -> (b -> c)`
- `(<$>) :: (a -> b) -> f a -> f b`
- `(<$>) :: (a -> (b -> c)) -> f a -> f (b -> c)`
- `(f <$>) :: f a -> f (b -> c)`

---

# Defining `liftA2` with `<$>` and `<*>` (4/4)

Let's follow the types:

- `(f <$>) :: f a -> f (b -> c)`
- `x :: f a`
- `(f <$> x) :: f (b -> c)`
- `<*> :: f (a -> b) -> f a -> f b`
- `<*> :: f (b -> c) -> f b -> f c`
- `y :: f b`
- `(f <$> x <*> y) :: f c`

---

# Maybe Instance

```haskell
instance Applicative Maybe where
  pure = Just
  Nothing  <*> _ = Nothing
  (Just f) <*> x = fmap f x
```

---

# [] Instance

```haskell
instance Applicative [] where
  pure x = [x]
  fs <*> xs = [f x | f <- fs, x <- xs]
```

---

# Homework (1/8)

```haskell
-- A parser for a value of type a is a function which takes a String
-- representing the input to be parsed, and succeeds or fails; if it
-- succeeds, it returns the parsed value along with the remainder of
-- the input.

newtype Parser a = Parser {runParser :: String -> Maybe (a, String)}
```

---

# Homework (2/8)

```haskell
satisfy :: (Char -> Bool) -> Parser Char
satisfy p = Parser f
  where
    f [] = Nothing -- fail on the empty input
    f (x : xs) -- check if x satisfies the predicate
    -- if so, return x along with the remainder
    -- of the input (that is, xs)
      | p x = Just (x, xs)
      | otherwise = Nothing -- otherwise, fail

char :: Char -> Parser Char
char c = satisfy (== c)
```

---

# Homework (3/8)

```haskell
posInt :: Parser Integer
posInt = Parser f
  where
    f xs
      | null ns = Nothing
      | otherwise = Just (read ns, rest)
      where
        (ns, rest) = span isDigit xs
```

---

# Homework (4/8)

```haskell
-- Exercise 1
-- First, you’ll need to implement a Functor instance for Parser.
-- Hint: You may find it useful to implement a function
-- first :: (a -> b) -> (a,c) -> (b,c)

first :: (a -> b) -> (a, c) -> (b, c)
first f (x, y) = (f x, y)

instance Functor Parser where
  fmap :: (a -> b) -> Parser a -> Parser b
  fmap f = Parser . fmap (fmap (first f)) . runParser
```

---

# Homework (5/8)

```haskell
-- Exercise 2
-- Now implement an Applicative instance for Parser

instance Applicative Parser where
  pure :: a -> Parser a
  pure x = Parser $ \s -> Just (x, s)
  (<*>) :: Parser (a -> b) -> Parser a -> Parser b
  p1 <*> p2 = Parser $ \s -> case runParser p1 s of
    Nothing -> Nothing
    Just (f, s') -> runParser (fmap f p2) s'

-- p1 <*> p2 =
--   Parser $
--     runParser p1 >=> \(f, s) -> runParser (fmap f p2) s
```

---

# Homework (6/8)

```haskell
-- Exercise 3

abParser :: Parser (Char, Char)
abParser = (,) <$> char 'a' <*> char 'b'

abParser_ :: Parser ()
abParser_ = void abParser

intPair :: Parser [Integer]
intPair = (<>) <$> posInt' <*> (char ' ' *> posInt')
  where
    posInt' = pure <$> posInt
```

---

# Homework (7/8)

```haskell
-- Exercise 4

instance Alternative Parser where
  empty = Parser $ const Nothing
  p1 <|> p2 = Parser $ \s -> runParser p1 s <|> runParser p2 s
```

---

# Homework (8/8)

```haskell
-- Exercise 5

intOrUppercase :: Parser ()
intOrUppercase = void posInt <|> void (satisfy isUpper)
```

---

# Next Week

# [fit] Semigroup
# [fit] Monoid
# [fit] Monad
