
module Calc where

-- https://www.cis.upenn.edu/~cis194/spring13/hw/05-type-classes.pdf
import ExprT
import Parser
import Data.Maybe
-- import StackVM

-- exercise 1

eval :: ExprT -> Integer 
eval (Lit x) = x
eval (Add e1 e2) = eval e1 + eval e2
eval (Mul e1 e2) = eval e1 * eval e2


-- exercise 2

evalStr :: String -> Maybe Integer 
evalStr s
    | isJust parsed = Just $ eval $ fromJust parsed
    | otherwise = Nothing
    where parsed = parseExp Lit Add Mul s

-- exercise 3

class Expr a where
    lit :: Integer -> a
    add :: a -> a -> a  
    mul :: a -> a -> a 

instance Expr ExprT where
  lit = Lit
  add = Add
  mul = Mul

reify :: ExprT -> ExprT 
reify = id

-- :t reify $ mul (add (lit 2) (lit 3)) (lit 4)  
-- reify $ mul (add (lit 2) (lit 3)) (lit 4) :: ExprT

-- exercise 4

instance Expr Integer where  
  lit x = x 
  add x y = x + y
  mul x y = x * y

instance Expr Bool where
  lit x
    | x<=0 = False 
    | otherwise = True
  add x y = x || y
  mul x y = x && y

newtype MinMax  = MinMax Integer deriving (Eq, Show)

instance Expr MinMax where
  lit x = MinMax x
  add (MinMax x) (MinMax y) = MinMax (max x y)
  mul (MinMax x) (MinMax y) = MinMax (min x y)

newtype Mod7    = Mod7 Integer deriving (Eq, Show)

instance Expr Mod7 where
  lit x = Mod7 (x `mod` 7) 
  add (Mod7 x) (Mod7 y) = Mod7 ((x+y) `mod` 7) 
  mul (Mod7 x) (Mod7 y) = Mod7 ((x*y) `mod` 7) 


testExp :: Expr a => Maybe a
testExp = parseExp lit add mul "(3*-4) + 5"
testInteger  = testExp :: Maybe Integer
testBool     = testExp :: Maybe Bool
testMM       = testExp :: Maybe MinMax
testSat      = testExp :: Maybe Mod7

-- exercise 5 OR 6

