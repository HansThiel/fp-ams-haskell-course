{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wall -Werror #-}

module JSON (JValue (..), JSONError, JSON, toJValue, fromJValue) where

import Data.Either (partitionEithers)
import Data.List (intercalate)

data JValue
  = JString String
  | JNumber Double
  | JBool Bool
  | JNull
  | JObject [(String, JValue)]
  | JArray [JValue]
  deriving (Eq, Ord, Show)

type JSONError = String

class JSON a where
  toJValue :: a -> JValue
  fromJValue :: JValue -> Either JSONError a

instance JSON Bool where
  toJValue = JBool
  fromJValue (JBool x) = Right x
  fromJValue x = leftShow "not a bool" x

instance JSON String where
  toJValue = JString
  fromJValue (JString x) = Right x
  fromJValue x = leftShow "not a string" x

instance JSON Integer where
  toJValue = JNumber . fromInteger
  fromJValue (JNumber x) = Right (truncate x)
  fromJValue x = leftShow "not a number" x

instance JSON Double where
  toJValue = JNumber
  fromJValue (JNumber x) = Right x
  fromJValue x = leftShow "not a number" x

instance (JSON a) => JSON [(String, a)] where
  toJValue = JObject . fmap (fmap toJValue)
  fromJValue (JObject xs) =
    case partitionEithers (fromJValue . snd <$> xs) of
      ([], ys) -> Right $ zip (fst <$> xs) ys
      (errs, _) -> Left $ intercalate "," errs
  fromJValue x = leftShow "not an object" x

instance {-# OVERLAPPABLE #-} (JSON a) => JSON [a] where
  toJValue = JArray . fmap toJValue
  fromJValue (JArray xs) =
    case partitionEithers (fromJValue <$> xs) of
      ([], ys) -> Right ys
      (errs, _) -> Left $ intercalate "," errs
  fromJValue x = leftShow "not an array" x

leftShow :: Show a => String -> a -> Either JSONError b
leftShow err x = Left $ err <> ": " <> show x
