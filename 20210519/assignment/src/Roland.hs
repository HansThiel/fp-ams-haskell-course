{-# LANGUAGE FlexibleInstances #-}

module Roland (JValue (..), JSON, toJValue, fromJValue) where

import Data.Bifunctor
import RolandHelpers

data JValue
  = JString String
  | JNumber Double
  | JBool Bool
  | JNull
  | JObject [(String, JValue)]
  | JArray [JValue]
  deriving (Eq, Ord, Show)

type JSONError = String

class JSON a where
  toJValue :: a -> JValue
  fromJValue :: JValue -> Either JSONError a

instance JSON Bool where
  toJValue = JBool
  fromJValue (JBool x) = Right x

instance JSON String where
  toJValue = JString
  fromJValue (JString x) = Right x

instance JSON Integer where
  toJValue = JNumber . fromIntegral
  fromJValue (JNumber x)
    | isInt x = Right (round x)
    | otherwise = Left "Cannot represent Double as Int"

instance JSON Double where
  toJValue = JNumber
  fromJValue (JNumber x) = Right x

instance (JSON a) => JSON [(String, a)] where
  toJValue = JObject . map (second toJValue)
  fromJValue (JObject xs) = mapM (sequencePair . second fromJValue) xs

instance {-# OVERLAPPABLE #-} (JSON a) => JSON [a] where
  toJValue = JArray . map toJValue
  fromJValue (JArray xs) = mapM fromJValue xs
