{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -Wall -Wno-incomplete-patterns #-}

module LogAnalysis where

import Log
import Text.Read (readMaybe)

{-

Something has gone terribly wrong! We’re really not sure what happened, but we
did manage to recover the log file error.log. It seems to consist of a different
log message on each line. Each line begins with a character indicating the type
of log message it represents:

- ’I’ for informational messages,
- ’W’ for warnings, and
- ’E’ for errors.

The error message lines then have an integer indicating the severity of the
error, with 1 being the sort of error you might get around to caring about
sometime next summer, and 100 being epic, catastrophic failure. All the types of
log messages then have an integer time stamp followed by textual content that
runs to the end of the line. Here is a snippet of the log file including an
informational message followed by a level 2 error message:

I 147 mice in the air, I’m afraid, but you might catch a bat, and
E 2 148 #56k istereadeat lo d200ff] BOOTMEM

It’s all quite confusing; clearly we need a program to sort through this mess.
We’ve come up with some data types to capture the struc- ture of this log file
format:

 data MessageType = Info
                  | Warning
                  | Error Int
                  deriving (Show, Eq)

 type TimeStamp = Int

 data LogMessage = LogMessage MessageType TimeStamp String
                 | Unknown String
                 deriving (Show, Eq)

Note that LogMessage has two constructors: one to represent normally- formatted
log messages, and one to represent anything else that does not fit the proper
format.

-}

{-

Exercise 1 The first step is figuring out how to parse an individual message.
Define a function:

 parseMessage :: String -> LogMessage

which parses an individual line from the log file. For example,

 parseMessage "E 2 562 help help"
     == LogMessage (Error 2) 562 "help help"

parseMessage "I 29 la la la"
     == LogMessage Info 29 "la la la"

 parseMessage "This is not in the right format"
     == Unknown "This is not in the right format"
-}

parseMessage :: String -> LogMessage
parseMessage msg = case parseMessageType msg of
  Nothing -> Unknown msg
  Just (msgType, msg') -> case parseInt msg' of
    Just (n, msg'') -> LogMessage msgType n msg''
    Nothing -> Unknown msg

parseMessage' :: String -> Maybe LogMessage
parseMessage' msg = do
  (msgType, msg') <- parseMessageType msg
  (n, msg'') <- parseInt msg'
  pure $ LogMessage msgType n msg''

type ParseResult a = Maybe (a, String)

parseMessageType :: String -> ParseResult MessageType
parseMessageType msg =
  case words msg of
    ("I" : ws) -> Just (Info, unwords ws)
    ("W" : ws) -> Just (Warning, unwords ws)
    ("E" : ws) -> case parseInt (unwords ws) of
      Just (n, msg') -> Just (Error n, msg')
      Nothing -> Nothing
    _ -> Nothing

parseInt :: String -> ParseResult Int
parseInt s = case words s of
  [] -> Nothing
  (w : ws) -> case readMaybe w :: Maybe Int of
    Just n -> Just (n, unwords ws)
    Nothing -> Nothing

{-
Once we can parse one log message, we can parse a whole log file. Define a
function

 parse :: String -> [LogMessage]

which parses an entire log file at once and returns its contents as a list of
LogMessages.
-}

parse :: String -> [LogMessage]
parse = map parseMessage . lines

{-
Exercise 2 Define a function

 insert :: LogMessage -> MessageTree -> MessageTree

which inserts a new LogMessage into an existing MessageTree, pro- ducing a new
MessageTree. insert may assume that it is given a sorted MessageTree, and must
produce a new sorted MessageTree containing the new LogMessage in addition to
the contents of the original MessageTree.

However, note that if insert is given
a LogMessage which is Unknown, it should return the MessageTree unchanged.
-}

insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) tree = tree
insert msg Leaf = Node Leaf msg Leaf
insert
  msg@(LogMessage _ t1 _)
  (Node left middle@(LogMessage _ t2 _) right)
    | t1 < t2 = Node (insert msg left) middle right
    | t1 >= t2 = Node left middle (insert msg right)

{-
Exercise 3 Once we can insert a single LogMessage into a MessageTree, we can
build a complete MessageTree from a list of messages. Specifi- cally, define a
function

 build :: [LogMessage] -> MessageTree

which builds up a MessageTree containing the messages in the list, by
successively inserting the messages into a MessageTree (beginning with a Leaf).
-}

build :: [LogMessage] -> MessageTree
build = foldr insert Leaf

{-
Exercise 4 Finally, define the function

 inOrder :: MessageTree -> [LogMessage]

which takes a sorted MessageTree and produces a list of all the LogMessages it
contains, sorted by timestamp from smallest to biggest. (This is known as an
in-order traversal of the MessageTree.)
-}

inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node left msg right) =
  inOrder left ++ [msg] ++ inOrder right

{-
Exercise 5 Now that we can sort the log messages, the only thing left to do is
extract the relevant information. We have decided that “relevant” means “errors
with a severity of at least 50”. Write a function

whatWentWrong :: [LogMessage] -> [String]

which takes an unsorted list of LogMessages, and returns a list of the messages
corresponding to any errors with a severity of 50 or greater, sorted by
timestamp. (Of course, you can use your functions from the previous exercises to
do the sorting.)
-}

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong = map content . filter isSevere . inOrder . build
  where
    isSevere :: LogMessage -> Bool
    isSevere (LogMessage (Error n) _ _) = n > 50
    isSevere _ = False
    content :: LogMessage -> String
    content (LogMessage _ _ s) = s
