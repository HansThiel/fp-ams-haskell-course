{-# LANGUAGE ScopedTypeVariables #-}

module Yoshikuni1b where

import Paths_homework
-- import Text.Read
import Data.List

-- https://adventofcode.com/2018/day/1


readInteger :: [Char] -> Integer
readInteger ('-':xs) = (read xs :: Integer) * (-1)
readInteger ('+':xs) = read xs :: Integer
readInteger string = read string :: Integer

findDuplicatedValue :: [Integer] -> [Integer] -> Integer
findDuplicatedValue accumulatedValues changes = 
    let newValue = last accumulatedValues + head changes
        isNewValueDuplicated = elem newValue accumulatedValues
    in if isNewValueDuplicated 
        then newValue
        else findDuplicatedValue (accumulatedValues ++ [newValue]) (drop 1 changes)

main :: IO ()
main = do
  file <- getDataFileName "data/1a.txt"
  input <- readFile file

  let changes = (fmap readInteger . lines) input

  let result = findDuplicatedValue [0] (cycle changes)
  print result
  pure ()


-- Some initial trials which didn't work

-- getLastOrZero :: [Integer] -> Integer
-- getLastOrZero [] = 0
-- getLastOrZero xs = last xs

-- getAccumulatedChange :: [Integer] -> [Integer]
-- getAccumulatedChange = foldl (\accumulatedChange change -> accumulatedChange ++ [getLastOrZero accumulatedChange + change] ) [] 

-- containsMultiple :: [Integer] -> [Bool]
-- containsMultiple array = foldl (\results element ->  ) [] array
--     fmap (\element -> elem element array) array

-- findDuplicatedElement :: [(Integer, Bool)] -> Maybe Integer
-- findDuplicatedElement array = fmap fst (find snd array)

-- goal :: [Integer] -> Maybe Integer
-- goal changes = 
--     let accumulatedChanges = getAccumulatedChange changes
--         zippedArray = zip accumulatedChanges (containsMultiple accumulatedChanges)
--     in findDuplicatedElement zippedArray