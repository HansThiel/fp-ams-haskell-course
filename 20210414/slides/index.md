# Haskell: From Beginner to Intermediate #10

## Semigroups and Monoids

### FP AMS 14/04/2021 

---

![right 100%](learning-curve.png)

# Overall Planning

1. Basics
2. Functors, Applicative Functors, and **Monoids**
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50%](book-cover.jpg)![inline 64%](book-of-monads.png)

---


![](brick-wall.jpg)

---

# Semigroup (1/n)

Abstract algebra: a set $$S$$ together with a binary operation $$\oplus$$

- $$S$$ is nonempty
- $$\oplus$$ is associative, i.e. $$ (x \oplus y) \oplus z = x \oplus (y \oplus z) $$
- $$S$$ is closed under $$\oplus$$

In Haskell:

- set $$\approx$$ type
-  `f :: a -> a -> a`, where `f (f x y) z == f x (f y z)`

---

# Semigroup (2/n)

```
λ> :t min
min :: Ord a => a -> a -> a

λ> :t max
max :: Ord a => a -> a -> a

λ> min 5 3
3

λ> max 5 3
5

Tλ> min 1 (min 2 3) == min (min 1 2) 3
True

λ> max 1 (max 2 3) == max (max 1 2) 3
True
```

---

# Semigroup (3/n)

```haskell
class Semigroup a where -- minimal: (<>)
  (<>) :: a -> a -> a
  sconcat :: NonEmpty a -> a
  stimes :: Integral b => b -> a -> a
```

---

# Monoid (1/n)

Abstract algebra: a semigroup with an identity element $$id$$, where

- $$id \oplus x = x$$
- $$x \oplus id = x$$

---

# Monoid (2/n)

```haskell
class Semigroup a => Monoid a where -- minimal: mempty
  mempty :: a
  mappend :: a -> a -> a
  mconcat :: [a] -> a
```
